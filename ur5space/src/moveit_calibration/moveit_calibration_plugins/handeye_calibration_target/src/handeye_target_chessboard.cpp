/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2020,  PickNik, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of PickNik, Inc., nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Yu Yan, John Stechschulte */

#include <moveit/handeye_calibration_target/handeye_target_chessboard.h>

namespace moveit_handeye_calibration
{
const std::string LOGNAME = "handeye_chessboard_target";

HandEyeChessboardTarget::HandEyeChessboardTarget()
{
  inner_corners_x_ = 6;
  inner_corners_y_ = 8;
  board_size_meters_x_ = 0.18;
  board_size_meters_y_ = 0.27;
  square_size_meters_ = 0.03;
}

bool HandEyeChessboardTarget::initialize()
{
  target_params_ready_ = true;
  return target_params_ready_;
}

bool HandEyeChessboardTarget::createTargetImage(cv::Mat& image) const
{
  if (!target_params_ready_)
    return false;
  else
    return true;
}


bool HandEyeChessboardTarget::detectTargetPose(cv::Mat& image)
{
  if (!target_params_ready_)
    return false;
  std::lock_guard<std::mutex> base_lock(base_mutex_);
  try
  {
    // Detect aruco board
    cv::Size pattern_size(inner_corners_x_, inner_corners_y_);

    std::vector<cv::Point2f> image_corners;   // this will be filled by the detected corners

    bool pattern_found = cv::findChessboardCorners(image, pattern_size, image_corners,
    cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK);

    cv::Mat image_rgb;
    cv::cvtColor(image, image_rgb, cv::COLOR_GRAY2RGB);
    // refine chessboard corners
    if(pattern_found)
    {
      cv::cornerSubPix(image, image_corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
      
      cv::drawChessboardCorners(image_rgb, pattern_size, cv::Mat(image_corners), pattern_found);

      // Create chessboard corners in 3D frame (board frame) - by construction
      std::vector<cv::Point3f> board_corners;
      for( int i = 0; i < pattern_size.height; ++i )
      {
        for( int j = 0; j < pattern_size.width; ++j )
        {
            board_corners.push_back(cv::Point3f(j*square_size_meters_, i*square_size_meters_, 0));
        }
      }

      cv::solvePnP(board_corners, image_corners, camera_matrix_, distortion_coeffs_, rotation_vect_, translation_vect_, false);
      drawAxis(image_rgb, camera_matrix_, distortion_coeffs_, rotation_vect_, translation_vect_, 0.05);
    }
    else
    {
        return false;
    }
    image = image_rgb;
  }
  catch (const cv::Exception& e)
  {
    ROS_ERROR_STREAM_THROTTLE_NAMED(1., LOGNAME, "Chessboard target detection exception: " << e.what());
    return false;
  }

  return true;
}

}  // namespace moveit_handeye_calibration
