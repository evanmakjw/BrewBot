#! /usr/bin/env python

import rospy
import geometry_msgs.msg
from move_group_interface import MoveGroupPythonInteface
from std_msgs.msg import Float32MultiArray
import gazebo_msgs.msg
from sensor_msgs.msg import Image, PointCloud2
from cv_bridge import CvBridge
import cv2
import numpy as np
import sensor_msgs.point_cloud2 as pc2
import math
from camera import Camera
from entities import entity
from brewbot2 import *
from std_msgs.msg import String
import sys

#Initialise queue for orders
orderQueue = []

def initialise(mode):       
    # Create an instance of the entire BrewBot system
    brewBot = BrewBot(mode)

    # Create instances of the ingredients
    milkJug = entity("Milk Jug")
    miloTin = entity("Milo Tin")
    coffeeTin = entity("Coffee Tin")
    spoon = entity("Spoon")
    mug = entity("Mug")
    sugarShaker = entity("Sugar Shaker")
    spoonHolder = entity("Spoon Holder")

    spoon.pose.position.x = -0.3
    spoon.pose.position.y = 0.6
    spoon.pose.position.z = 0.19

    mug.pose.position.x = 0.1
    mug.pose.position.y = 0.6
    mug.pose.position.z = 0.115

    coffeeTin.pose.position.x = 0.3
    coffeeTin.pose.position.y = 0.3
    coffeeTin.pose.position.z = 0.17 
    
    milkJug.pose.position.x = -0.2
    milkJug.pose.position.y = 0.8
    milkJug.pose.position.z = 0.15276
    
    sugarShaker.pose.position.x = -0.3
    sugarShaker.pose.position.y = 0.4
    sugarShaker.pose.position.z = 0.214

    miloTin.pose.position.x = 0.3
    miloTin.pose.position.y = 0.5
    miloTin.pose.position.z = 0.17 

    # Add the instances to the system
    brewBot.addEntity(sugarShaker)
    brewBot.addEntity(milkJug)
    brewBot.addEntity(coffeeTin)
    brewBot.addEntity(miloTin)
    brewBot.addEntity(mug)
    brewBot.addEntity(spoon)
    brewBot.addEntity(spoonHolder)   

    # Create MoveGroupPythonInteface in order to move the RG2 Gripper
    myMgpi = MoveGroupPythonInteface()
    brewBot.addMgpi(myMgpi)

    return brewBot


def ordersCallback(data):
    # Add order to the queue
    orderQueue.append(data)

def main():


    print("Options:")
    print("1: GUI Mode")
    print("2: Debugger Mode ")
    print("------------------\n")

    command = raw_input("Enter Option: " )

    # Enter GUI Mode
    if command == '1':
        mode = True
        brewBot = initialise(mode)

        # Create subsciber for orders
        orderSubcriber = rospy.Subscriber("/orders", String, ordersCallback)
        
        while(True): 
            # If queue is not empty
            if orderQueue: 
                print("Current Order:")
                print(orderQueue[0].data)

                orderStrings = orderQueue[0].data.split(" ")

                raw_input("Press Enter to confirm order: ")

                # Scan for locations of objects
                scanResult = brewBot.scan()

                if not scanResult:
                    print("Calibration unsuccessful, ending program.")
                    sys.exit(0)

                if orderStrings[1] == 'SUGAR':
                    sugar = True 
                else:
                    sugar = False

                # Send Coffee to Robot Barista
                brewBot.makeDrink(orderStrings[0], sugar)
                
                # Remove order fron the queue
                orderQueue.pop(0)
                
        rospy.spin() 

    # Enter Debugger Mode1
    elif command == '2': 
        mode = True
        brewBot = initialise(mode)

        while(True):
            print("\nBeverage Options:")
            print("1: Coffee")
            print("2: Hot Milo")
            print("3: Quit Program")
            print("------------------\n")

            beverage = raw_input("Enter Option: " )

            print('{} selected.\n'.format(beverage))

            if beverage == '1': 

                sugar = raw_input("Press [1] for sugar, [2] for no sugar: ")

                if sugar == '1':
                    sugar = True
                    print("Sugar selected.")

                else:
                    sugar = False
                    print("No sugar selected.")


                # Send Coffee to BrewBot
                print("Coffee order confirmed. BrewBot Initiated.")

                # Scan for locations of objects
                scanResult = brewBot.scan()

                if not scanResult:
                    print("Calibration unsuccessful, ending program.")
                    sys.exit(0)

                # Send Coffee to Robot Barista
                brewBot.makeDrink('COFFEE', sugar)
                
            elif beverage == '2':

                sugar = raw_input("Press [1] for sugar, [2] for no sugar: ")

                if sugar == '1':
                    sugar = True
                    print("Sugar selected.")

                else:
                    sugar = False
                    print("No sugar selected.")

                
                # Send Hot Milo to BrewBot
                print("Milo order confirmed. BrewBot Initiated.")

                # Scan for locations of objects
                scanResult = brewBot.scan()

                if not scanResult:
                    print("Calibration unsuccessful, ending program.")
                    sys.exit(0)

                # Send Coffee to Robot Barista
                brewBot.makeDrink('MILO', sugar)

            elif beverage == 'q':
                sys.exit(0)

            else:
                print("Invalid Option. Try again bitch.")

        rospy.spin() 






    # Create an instance of the Intel RealSense D435
    # brewCamera = Camera()
    # if True:
    # exit()

    # brewBot.addCamera(brewCamera)


   
    # Take a scan of the ingredients on the table and get their coordinates
    # brewBot.scan()


if __name__ == '__main__':
    # rospy.init_node('master_node')
    main()
    
                