#! /usr/bin/env python

import rospy
import geometry_msgs.msg
from move_group_interface import MoveGroupPythonInteface
from std_msgs.msg import Float32MultiArray
import gazebo_msgs.msg
from sensor_msgs.msg import Image, PointCloud2
from cv_bridge import CvBridge
import cv2
import numpy as np
import sensor_msgs.point_cloud2 as pc2
from scipy.spatial.transform import Rotation as R
import math
from camera import Camera
from entities import entity
import moveit_commander
from gazebo_msgs.msg import ModelStates

SUGAR = 0
JUG = 1
COFFEE = 2
MILO = 3
MUG = 4
SPOON = 5
SPOONHOLDER = 6

STRAIGHT = 0
SCOOP = 1
FLAT = 2
POUR = 3

class BrewBot:

    # Initialises all relevant data
    def __init__(self, debug):

        self.entities = []
        self.camera = None
        self.objectStateSub = rospy.Subscriber('/objectStates', ModelStates, self.objectStateCallback)
        self.updateData = False
        self.mgpi = None
        self.dataReceived = False

        self.positionX = 0 
        self.positionY = 0
        self.positionZ = 0

        self.orientationX = 0.366109557881         # Gripper faces down
        self.orientationY = 0.604687882539
        self.orientationZ = -0.353739717
        self.orientationW = 0.612523117078

        self.startState = [ -2.171126667653219, 
                            -1.864784065877096, 
                            -0.9486864248858851, 
                            -1.9012678305255335, 
                            1.5678542852401733, 
                            -0.5834081808673304 ]

        self.sugarState = [ 0.802375316619873, 
                            -1.6356914679156702, 
                            1.8780879974365234, 
                            -0.2680881659137171, 
                            1.6314021348953247, 
                            -0.8353832403766077]

        self.debug = debug
        self.currentDrink = ""

    def objectStateCallback(self, msg):

        self.dataReceived = True

        # Check if user wants to update the position of the ingredients
        if self.updateData == True:
            for i in range(0, len(msg.name)):
                if msg.name[i] == 'Milk Jug':
                    self.entities[JUG].setData(msg.pose[i])
                elif msg.name[i] == 'Coffee Tin':
                    self.entities[COFFEE].setData(msg.pose[i])
                elif msg.name[i] == 'Milo Tin':
                    self.entities[MILO].setData(msg.pose[i])
                elif msg.name[i] == 'Mug':
                    self.entities[MUG].setData(msg.pose[i])
                elif msg.name[i] == 'Spoon':
                    self.entities[SPOON].setData(msg.pose[i])
                    self.entities[SPOONHOLDER].setData(msg.pose[i])
                elif msg.name[i] == 'Sugar Shaker':
                    self.entities[SUGAR].setData(msg.pose[i])  
            self.updateData = False

        else:
            pass   


    # Update the stored pose of the end effector
    def updatePose(self):

        # Get the current pose
        pose = self.mgpi.get_current_eef_pose()

        # Update the position
        self.positionX = pose.pose.position.x
        self.positionY = pose.pose.position.y
        self.positionZ = pose.pose.position.z

        # Update the orientation
        self.orientationX = pose.pose.orientation.x        
        self.orientationY = pose.pose.orientation.y
        self.orientationZ = pose.pose.orientation.z
        self.orientationW = pose.pose.orientation.w

    # Add a camera to the system
    def addCamera(self, camera):
        self.camera = camera

    # Add an entity to the system
    def addEntity(self, entity):
        self.entities.append(entity)

    # Add a control interface to the system
    def addMgpi(self, mgpi):
        self.mgpi = mgpi

    # Take a scan of all the ingredients on the table
    def scan(self):

        # set flag so object location data is updated in callback
        self.updateData = True

        while True:

            # if new data has not been received restart loop
            if not self.dataReceived:
                continue
            
            # print poses of objects
            print('Poses')
            for entity in self.entities:
                print('Name: ')
                print(entity.name)
                print('pose: ')
                print(entity.pose)

            # request for input
            userInput = raw_input('Press [1] to confirm poses, [2] to rescan, or [3] to end program: ')

            # poses correct
            if userInput == '1':

                print('Poses confirmed')

                # intialise collision objects
                self.mgpi.create_stage(self.entities)

                # reset object location update flag
                self.updateData = False

                # return successful calibration result
                return True
            
            # redo scan
            elif userInput == '2':
                print('Performing rescan')
                self.dataReceived = False
                self.updateData = True
                continue

            # calibration is unsuccessful
            elif userInput == '3':
                self.updateData = False
                return False

    # Transformation coordinates by a rotation to account for arm offset on table
    def performTransform(self, trans):

        desiredPose = np.identity(4)
        desiredPose[0:3,3] = trans
        
        # get offset rotation
        r = R.from_rotvec([0, 0, -(np.pi)/4])
        offsetRotation = r.as_dcm()
        offsetMatrix = np.identity(4)
        offsetMatrix[0:3,0:3] = offsetRotation

        # new pose:
        adjustedPose = np.matmul(offsetMatrix, desiredPose)

        return adjustedPose[0:3,3]


    # Move the end effector to the desired location. Changes position only
    def changePosition(self, target, constrained=False, xOffset=0, yOffset=0, zOffset=0):

        print(target.name)
        print("Updating")
        # update pose of end effector
        self.updatePose()

        print("Updated")

        # Translation vector
        trans = []

        # offsets account for positioning above or to the side of an object
        trans.append(target.pose.position.x + xOffset)
        trans.append(target.pose.position.y + yOffset)
        trans.append(target.pose.position.z + zOffset)

        print('trans', trans)

        # Transform vector to account for offset
        adjustedPose = self.performTransform(trans)

        print("Adjusted Pose")
        print(adjustedPose)

        # Final pose
        targetPose = geometry_msgs.msg.Pose()

        # Transfer data from transformed pose
        targetPose.position.x = adjustedPose[0]
        targetPose.position.y = adjustedPose[1]
        targetPose.position.z = adjustedPose[2]

        # Update the current position
        # self.positionX = targetPose.position.x
        # self.positionY = targetPose.position.y
        # self.positionZ = targetPose.position.z

        # Orientation remains the same
        targetPose.orientation.x = self.orientationX
        targetPose.orientation.y = self.orientationY
        targetPose.orientation.z = self.orientationZ
        targetPose.orientation.w = self.orientationW

        print(targetPose)

        # Move to initial position
        if constrained:
            self.mgpi.move_eef_to_pose_constrained(targetPose)
           
        else:
            self.mgpi.move_eef_to_pose(targetPose)

    # Change position and orientation of end effector at the same time
    def changePose(self, target, angle, xOffset=0, yOffset=0, zOffset=0):

        # update pose of end effector
        self.updatePose()

        # Translation vector
        trans = []

        # offsets account for positioning above or to the side of an object
        trans.append(target.pose.position.x + xOffset)
        trans.append(target.pose.position.y + yOffset)
        trans.append(target.pose.position.z + zOffset)

        # Transform vector to account for offset
        adjustedPose = self.performTransform(trans)

        # Final pose
        targetPose = geometry_msgs.msg.Pose()

        # Transfer data from transformed pose
        targetPose.position.x = adjustedPose[0]
        targetPose.position.y = adjustedPose[1]
        targetPose.position.z = adjustedPose[2]

        # Gripper should be pointing straight down
        if angle == STRAIGHT:
            targetPose.orientation.x = -0.49579354163
            targetPose.orientation.y = 0.504459456163
            targetPose.orientation.z = 0.496265406461
            targetPose.orientation.w = 0.503418382179

        # Gripper should be at a 45 degree-ish angle
        elif angle == SCOOP:
            targetPose.orientation.x = -0.801990397075                     # HARDCODE THESE LATER
            targetPose.orientation.y = 0.412481091964
            targetPose.orientation.z = 0.0436728450531
            targetPose.orientation.w = 0.429841173431

        # Gripper should be flat horizontally
        elif angle == FLAT:
            targetPose.orientation.x = 0.860875819512
            targetPose.orientation.y = 0.349646729471
            targetPose.orientation.z = 0.150332471789
            targetPose.orientation.w = 0.337698291195

        # Gripper should be flat horizontally but the other way
        elif angle == POUR:
            targetPose.orientation.x = None                                # HARDCODE THESE LATER
            targetPose.orientation.y = None
            targetPose.orientation.z = None
            targetPose.orientation.w = None

        
        self.mgpi.move_eef_to_pose(targetPose)

    
    # Change orientation of end effector only
    def changeOrientation(self, angle):

        # update pose of end effector
        self.updatePose()

        # Create a Pose
        targetPose = geometry_msgs.msg.Pose()

        # The position stays the same
        # MOVE THE SPOON BACK AS IT SCOOPS IF ANGLE IS 45 DEG
        targetPose.position.x = self.positionX
        targetPose.position.y =  self.positionY
        targetPose.position.z = self.positionZ

        # Gripper should be pointing straight down
        if angle == STRAIGHT:
            targetPose.orientation.x = -0.49579354163
            targetPose.orientation.y = 0.504459456163
            targetPose.orientation.z = 0.496265406461
            targetPose.orientation.w = 0.503418382179

        # Gripper should be at a 45 degree-ish angle
        elif angle == SCOOP:
            targetPose.orientation.x = -0.801990397075                     # HARDCODE THESE LATER
            targetPose.orientation.y = 0.412481091964
            targetPose.orientation.z = 0.0436728450531
            targetPose.orientation.w = 0.429841173431

            # Move Spoon back for optimal tilt
            targetPose.position.x -= 0.3

        # Gripper should be flat horizontally
        elif angle == FLAT:
            targetPose.orientation.x = 0.860875819512
            targetPose.orientation.y = 0.349646729471
            targetPose.orientation.z = 0.150332471789
            targetPose.orientation.w = 0.337698291195

        # Tilt for pouring milk into cup
        elif angle == POUR:
            targetPose.orientation.x = -0.674732023088                               # HARDCODE THESE LATER
            targetPose.orientation.y = 0.487863863005
            targetPose.orientation.z = 0.289282733489
            targetPose.orientation.w = 0.472272218428

        self.mgpi.move_eef_to_pose(targetPose)

    # rotates the end effector (eg to pour milk, coffee etc)
    def rotateEEF(self, target):
        if target.name == 'Spoon': 
            joints = self.mgpi.get_current_joint_state()
            joints[5] = joints[5] - 3.14
            try: 
                self.mgpi.move_to_joint_state(joints)
            except moveit_commander.exception.MoveItCommanderException:
                joints = self.mgpi.get_current_joint_state()
                joints[5] = joints[5] + 3.14
                self.mgpi.move_to_joint_state(joints)

        elif target.name == 'Sugar Shaker': 
            joints = self.mgpi.get_current_joint_state()
            joints[5] = joints[5] - 2.5
            try: 
                self.mgpi.move_to_joint_state(joints)
            except moveit_commander.exception.MoveItCommanderException:
                joints = self.mgpi.get_current_joint_state()
                joints[5] = joints[5] + 2.5
                self.mgpi.move_to_joint_state(joints)
        
        elif target.name == 'Milk Jug':
            joints = self.mgpi.get_current_joint_state()
            old_ee_joint = joints[5]  
            joints[5] = 0.059 # ee state for pouring milk

            if self.debug:
                raw_input("\nPress Enter to tip milk into mug..") 
            self.mgpi.move_to_joint_state(joints)

            joints[5] = old_ee_joint

            if self.debug:
                raw_input("\nPress Enter to tip milk jug back..") 
            self.mgpi.move_to_joint_state(joints)


    def getSpoon(self):

        # Move above spoon
        if self.debug:
            raw_input('Press Enter to move above spoon')
        self.changePosition(self.entities[SPOON], zOffset=0.32) # 0.32 may have to be changed

        # Lower arm
        if self.debug:
            raw_input('Press Enter to move down to spoon')
        self.changePosition(self.entities[SPOON], zOffset=0.18) # 0.3 may have to be changed

        # Grab Spoon
        if self.debug:
            raw_input('Press Enter to close gripper')
        self.mgpi.close_gripper(self.entities[SPOON].name)

        # Move back up
        if self.debug:
            raw_input('Press Enter to move back up with spoon')
        self.changePosition(self.entities[SPOON], zOffset=0.33) # 0.6 may have to be changed

    def scoopPowder(self, powder):

        print('----------------------- scoopPowder: Disable Collisions -----------------------') 
        print("Object Collisions: {}, States: {}", self.mgpi.scene.allowed_collision_matrix.entry_names, self.mgpi.scene.allowed_collision_matrix.entry_values)
        
        collisionObj = self.currentDrink.lower().capitalize() + " Tin"

        print(collisionObj)

        # Move to coffee/milo tin
        if self.debug:
            raw_input('Press Enter to move to coffee tin')
            # Disable collisions
            # index1 = self.mgpi.scene.allowed_collision_matrix.entry_names()
            # index2 = self.mgpi.scene.allowed_collision_matrix.entry_names
        self.changePosition(self.entities[powder], yOffset=0.02, zOffset=0.428)

        # Lower down the coffee tin
        if self.debug:
            raw_input('Press Enter to lower into coffee tin')
        self.changePosition(self.entities[powder], constrained=True, zOffset=0.278)

        # Tilt spoon and move backwards
        if self.debug:
            raw_input('Press Enter to move back up and tilt spoon')
        self.changePose(self.entities[powder], angle=SCOOP, xOffset =-0.3, yOffset=0.02, zOffset=0.4)

        # Re-enable collisions
        print('----------------------- scoopPowder: Enable Collisions -----------------------') 
        print("Object Collisions: {}, States: {}", self.mgpi.scene.allowed_collision_matrix.entry_names, self.mgpi.scene.allowed_collision_matrix.entry_values)

    def pourPowder(self):
        
        # Move to mug
        if self.debug:
            raw_input('Press Enter to move to mug')
        self.changePosition(self.entities[MUG], constrained=True, xOffset=-0.3, zOffset=0.335)

        # Move down to mug
        if self.debug:
            raw_input('Press Enter to move down to mug')
        self.changePosition(self.entities[MUG], constrained=True, xOffset=-0.3, zOffset=0.26)

        # Rotate eef to pour powder into mug
        if self.debug:
            raw_input('Press Enter to tip coffee into mug')
        self.rotateEEF(self.entities[SPOON]) 

    def returnSpoon(self):

        # Move spoon back above spoon holder
        if self.debug:
            raw_input('Press Enter to move back up with spoon')
        self.changePose(self.entities[SPOON], angle=STRAIGHT, zOffset=0.32)

        # Move down to put spoon in holder
        if self.debug:
            raw_input('Press Enter to move down into spoon holder')
        self.changePosition(self.entities[SPOON], constrained=True, zOffset=0.18)

        if self.debug:
            raw_input('Press Enter to open gripper')
        self.mgpi.open_gripper(self.entities[SPOON].name)

        # Move up
        if self.debug:
            raw_input('Press Enter to move back up')
        self.changePosition(self.entities[SPOON], constrained=True, zOffset=0.25)


    def addMilk(self):

        print('----------------------- addMilk: Disable Collisions -----------------------') 
        print("Object Collisions: {}, States: {}", self.mgpi.scene.allowed_collision_matrix.entry_names, self.mgpi.scene.allowed_collision_matrix.entry_values)

        

        # Move to milk jug
        if self.debug:
            raw_input('Press Enter to move to milk jug')
        self.changePose(self.entities[JUG], angle=FLAT, yOffset=-0.2, zOffset=-0.02576)

        # Pick up milk jug
        if self.debug:
            raw_input('Press Enter to close gripper')
        self.mgpi.close_gripper(self.entities[JUG].name)

        # Move milk jug up with constraints
        if self.debug:
            raw_input('Press Enter to move up with milk jug')
        self.changePosition(self.entities[JUG], constrained=True, yOffset=-0.2, zOffset=0.14724)

        # Move milk jug to mug with constraints
        if self.debug:
            raw_input('Press Enter to move to mug')
        self.changePosition(self.entities[MUG], constrained=True, xOffset=-0.1, yOffset=-0.2, zOffset=0.186)

        # Pour milk into mug
        self.rotateEEF(self.entities[JUG])
        
        # Return milk jug
        if self.debug:
            raw_input('Press Enter return milk jug')
        self.changePosition(self.entities[JUG], constrained=True, yOffset=-0.2, zOffset=0.14724)

        # Place milk jug down
        if self.debug:
            raw_input('Press Enter move milk jug down')
        self.changePosition(self.entities[JUG], constrained=True, yOffset=-0.2, zOffset=-0.02276)

        # release milk jug
        if self.debug:
            raw_input('Press Enter to open gripper')
        self.mgpi.open_gripper(self.entities[JUG].name)

        # Move back up
        if self.debug:
            raw_input('Press Enter move back up')
        self.changePosition(self.entities[JUG], constrained=True, yOffset=-0.2, zOffset=0.14724)

        print('----------------------- addMilk: Enable Collisions -----------------------') 
        print("Object Collisions: {}, States: {}", self.mgpi.scene.allowed_collision_matrix.entry_names, self.mgpi.scene.allowed_collision_matrix.entry_values)

    def addSugar(self):

        # Move to starting position for sugar shaker
        if self.debug:
            raw_input('Press Enter to move to starting position for sugar')
        self.mgpi.move_to_joint_state(self.sugarState)
        
        # Move above sugar shaker
        if self.debug:
            raw_input('Press Enter to move to sugar shaker')
        self.changePose(self.entities[SUGAR], angle=FLAT, yOffset=-0.185, zOffset=0.1)

        # Move down to sugar shaker
        if self.debug:
            raw_input('Press Enter to move down to sugar shaker')
        self.changePosition(self.entities[SUGAR], constrained=True, yOffset=-0.185, zOffset=-0.104)

        # Grab sugar
        if self.debug:
            raw_input('Press Enter to grip the sugar shaker')
        self.mgpi.close_gripper(self.entities[SUGAR].name)

        # Move it up
        if self.debug:
            raw_input('Press Enter to move up')
        self.changePosition(self.entities[SUGAR], constrained=True, yOffset=-0.185, zOffset=0.086)

        # Move to mug
        if self.debug:
            raw_input('Press Enter to move to the mug')
        self.changePosition(self.entities[MUG], constrained=True, xOffset=-0.07, yOffset=-0.2, zOffset=0.1228)

        # Shake sugar
        if self.debug:
            raw_input('Press Enter to shake the sugar')
        self.rotateEEF(self.entities[SUGAR])

        if self.debug:
            raw_input('Press Enter to make sugar upright again')
        self.rotateEEF(self.entities[SUGAR])

        # Move back to original sugar spot
        if self.debug:
            raw_input('Press Enter to move the shaker back')
        self.changePosition(self.entities[SUGAR], constrained=True, yOffset=-0.185, zOffset=0.086)

        # Move down
        if self.debug:
            raw_input('Press Enter to move the shaker down')
        self.changePosition(self.entities[SUGAR], constrained=True, yOffset=-0.185, zOffset=-0.09)

        if self.debug:
            raw_input('Press Enter to release the shaker')
        self.mgpi.open_gripper(self.entities[SUGAR].name)

        # Move back up
        if self.debug:
            raw_input('Press Enter to move back up')
        self.changePosition(self.entities[SUGAR], constrained=True, yOffset=-0.185, zOffset=0.086)

    def stir(self):

        # Move to Mug
        if self.debug:
            raw_input('Press Enter to move to the mug')
        self.changePosition(self.entities[MUG], zOffset=0.4)

        # Move down into Mug
        if self.debug:
            raw_input('Press Enter to move into the mug')
        self.changePosition(self.entities[MUG], constrained=True, zOffset=0.275)

        # stir the coffee
        if self.debug:
            raw_input('Press Enter to stir')
        self.rotateEEF(self.entities[SPOON])
        self.rotateEEF(self.entities[SPOON])

        # Move back up
        if self.debug:
            raw_input('Press Enter to move above mug')
        self.changePosition(self.entities[MUG], constrained=True, zOffset=0.4)


    def makeDrink(self, drink, sugar):

        self.currentDrink = drink

        # Open gripper
        if self.debug:
            raw_input('Press Enter to open gripper')
        self.mgpi.open_gripper(self.entities[SPOON].name)

        # Move to start position
        if self.debug:
            raw_input('Press Enter to move to start state')
        self.mgpi.move_to_joint_state(self.startState)

        # Get spoon
        self.getSpoon()

        if drink == 'COFFEE':
            # Scoop powder (Coffee)
            self.scoopPowder(COFFEE)

        elif drink == 'MILO':
            # Scoop powder (Milo)
            self.scoopPowder(MILO)

        # Pour powder into mug
        self.pourPowder()

        # Return spoon to spoon holder
        self.returnSpoon()

        # Move back to starting position
        if self.debug:
            raw_input('Press Enter to move back to starting position')
        self.mgpi.move_to_joint_state(self.startState)

        # Get milk and pour into mug
        self.addMilk()

        # If sugar has been requested get sugar and pour into mug
        if sugar:
            self.addSugar()

        # Move back to start state
        if self.debug:
            raw_input('Press Enter to go to start state')
        self.mgpi.move_to_joint_state(self.startState)

        # Get spoon
        self.getSpoon()

        # Stir drink
        self.stir()
        
        # Return spoon to spoon holder
        self.returnSpoon()

        # Return to home state
        if self.debug:
            raw_input('Press Enter to move back to starting position')
        self.mgpi.move_to_joint_state(self.startState)