#! /usr/bin/env python

import rospy
import geometry_msgs.msg
from move_group_interface import MoveGroupPythonInteface
from std_msgs.msg import Float32MultiArray
import gazebo_msgs.msg
from sensor_msgs.msg import Image, PointCloud2
from cv_bridge import CvBridge
import cv2
import numpy as np
import sensor_msgs.point_cloud2 as pc2
import math

# To represent the objects / ingredients on the table
class entity:

    def __init__(self, name):
        self.name = name
        self.pose = geometry_msgs.msg.Pose()

    def setData(self, pose):
        # print('pose received in setData function:')
        # print(self.name)
        # print(pose)
        self.pose.position.x = pose.position.x
        self.pose.position.y = pose.position.y
        # self.pose.position.z = pose.position.z
