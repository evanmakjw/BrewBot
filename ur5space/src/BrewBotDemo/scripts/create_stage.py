#! /usr/bin/env python

# Author: Tejaswi Digumarti (tejaswi.digumarti@sydney.edu.au)
# Description: This is the file that creates the robot arena for assignment 1.

# from ur5space.src.BrewBotDemo.scripts.GlobalVariables import COFFEE_TIN_HEIGHT, MILO_TIN_HEIGHT
import rospy
import geometry_msgs.msg
from gazebo_attach import GazeboLinkAttacher
from GlobalVariables import *
from scipy.spatial.transform import Rotation as R
import numpy as np
import os

class CreateStage(object):
	"""
	This class contains functions to create the stage for the planning scene.
	It also has utilities to to attach and detach objects in the planning scene and in Gazebo.
	"""

	def __init__(self, scene_interface, robot, eef_link):
		"""
		Initializes the object

		:param scene_interface: A PlanningSceneInterface object
		:param robot: The robot
		:param eef_link: Name of the end effector link
		"""
		super(CreateStage, self).__init__()
		self.scene_interface = scene_interface 			# PlanningSceneInterface
		self.robot = robot 			# Robot
		self.eef_link = eef_link 	# Name of the end effector link
		self.gazebo_attacher = GazeboLinkAttacher()
		# self.tempPoses = { 
		# 	'CoffeeTin': 	([0.3,  0.7, 0],	[0,0,0,0]),
		# 	'SugarShaker': 	([-0.3, 0.4, 0], 					[0,0,0,0]),
		# 	'Mug': 			([-0.1, 0.4, 0],					[0,0,0,0]),
		# 	'MiloTin': 		([0.3, 0.5, 0],		[0,0,0,0]),
		# 	'MilkJug': 		([-0.2, 0.8, 0],					[0,0,0,0]),					
		# 	'Spoon':		([-0.3, 0.6, 0], 				[0,0,0,0]),
		# 	'SpoonHolder':	None
			# 'MilkJug': 		None,
			# 'CoffeeTin': 	((	)			(		)), 
			# 'CoffeeTin': 	((	)			(		)), 

		# }

	def wait_for_scene_update(self, name, box_is_known=False, box_is_attached=False, timeout=4.0):
		"""
		Ensures that collision updates are received.

		If the Python node dies before publishing a collision object update message, the message
		could get lost and the box will not appear. To ensure that the updates are 
		made, we wait until we see the changes reflected in the 
		``get_attached_objects()`` and ``get_known_object_names()`` lists. 
		For the purpose of this tutorial, we call this function after adding, 
		removing, attaching or detaching an object in the planning scene. We then wait 
		until the updates have been made or ``timeout`` seconds have passed
		
		:param name: Name of the object that is being updated in the scene
		:param box_is_known: Boolean indicating if the box is part of the known objects
		:param box_is_attached: Boolean indicating if the box is attached
		:param timeout: Number of seconds to wait for scene update
		"""
		start = rospy.get_time()
		seconds = rospy.get_time()
		while (seconds - start < timeout) and not rospy.is_shutdown():
			# Test if the box is in attached objects
			attached_objects = self.scene_interface.get_attached_objects([name])
			is_attached = len(attached_objects.keys()) > 0
			# Test if the box is in the scene.
			# Note that attaching the box will remove it from known_objects
			is_known = name in self.scene_interface.get_known_object_names()
			# Test if we are in the expected state
			if (box_is_attached == is_attached) and (box_is_known == is_known):
				return True
			# Sleep so that we give other threads time on the processor
			rospy.sleep(0.1)
			seconds = rospy.get_time()

		# If we exited the while loop without returning then we timed out
		return False

	def add_block(self, size, pose_stamped, name, timeout=4):
		"""
		Adds a block to the scene.

		:param size: Dimensions of the block in metres as a tuple (sx, sy, sz)
		:type size: tuple
		:param pose_stamped: Pose of the block in 3D wrt
		:type pose_stamped: geometry_msgs.msg.PoseStamped
		:param name: Name of the block
		:type name: string
		:param timeout: Number of seconds to wait for scene update
		:type timeout: float
		"""
		self.scene_interface.add_box(name, pose_stamped, size)
		return self.wait_for_scene_update(name, box_is_known=True, timeout=timeout)

	def attach_object(self, name, timeout=4):
		"""
		Attaches objects to the robot

		:param name: Name of the box to attach
		:type name: string
		:param timeout: Number of seconds to wait for scene update
		:type timeout: float
		"""
		# Get the links in the endeffector group. These links can collide with the objects
		grasping_group = 'endeffector'
		touch_links = self.robot.get_link_names(group=grasping_group)
		# Attach objects in RViz
		self.scene_interface.attach_box(self.eef_link, name, touch_links=touch_links)
		# Attach objects in gazebo
		self.gazebo_attacher.attach_objects("robot", name, "wrist_3_link", "link")

		# We wait for the planning scene to update.
		return self.wait_for_scene_update(name, box_is_attached=True, box_is_known=False, timeout=timeout)

	def detach_object(self, name, timeout=4):
		"""
		Detaches objects from the Robot

		:param name: Name of the object to detach
		:type name: string
		:param timeout: Number of seconds to wait for scene update
		:type timeout: float
		"""
		# Detach objects in RViz
		self.scene_interface.remove_attached_object(self.eef_link, name=name)
		# Detach objects in gazebo
		self.gazebo_attacher.detach_objects("robot", name, "wrist_3_link", "link")
		# We wait for the planning scene to update.
		return self.wait_for_scene_update(name, box_is_known=True, box_is_attached=False, timeout=timeout)

	def remove_object(self, name, timeout=4):
		"""
		Removes specified object from the scene

		:param name: Name of the object to remove
		:type name: string
		:param timeout: Number of seconds to wait for scene update
		:type timeout: float
		"""
		# if the box it attached throw warning and detach the object
		if len(self.scene_interface.get_attached_objects([name]).keys()) > 0:
			print("Object named ", name, " is stilled attached. Detaching it.")
			self.scene_interface.detach_box(name)

		# **Note:** The object must be detached before we can remove it from the world
		self.scene_interface.remove_world_object(name)

		# We wait for the planning scene to update.
		return self.wait_for_scene_update(name, box_is_attached=False, box_is_known=False, timeout=timeout)

	def build_stage(self, entities):
		# Randomly build stage, return x,y,z positions for blocks and goal
		block_dim = 0.08									# length of the side of the block in m
		block_size = (block_dim, block_dim, block_dim)		# size of the block as a 3-tuple
		# cylinder_radius = 0.04								# radius of the obstacle cylinder
		# cylinder_height = 1.0								# height of the obstacle cylinder
		table_width = 0.8									# width of the table
		table_length = 1.5									# length of the table
		small_z = 0.001										# just a small number to use boxes as planes

		obj_pose = geometry_msgs.msg.PoseStamped()
		obj_pose.header.frame_id = "world"
		# obj_pose.pose.orientation.w = 1.0

		positions, orientations = self.transformToRobot([0,0.328,small_z/2])

		# add the table surface
		obj_pose.pose.position.x = positions[0]
		obj_pose.pose.position.y = positions[1]		# do not modify this value
		obj_pose.pose.position.z = positions[2]
		# print(positions)
		print(orientations)
		obj_pose.pose.orientation.x = orientations[0]
		obj_pose.pose.orientation.y = orientations[1]
		obj_pose.pose.orientation.z = orientations[2]
		obj_pose.pose.orientation.w = orientations[3]
		
		self.add_block((table_width, table_length, small_z), obj_pose, "table")

		position_x, position_y, objectNames = self.add_meshes(entities)

		return position_x, position_y, objectNames

	def add_meshes(self, entities):
		position_x = []
		position_y = []
		objectNames =[]
		modelpath = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'models', 'meshes')
		small_z = 0.001		

		obj_pose = geometry_msgs.msg.PoseStamped()
		obj_pose.header.frame_id = "world"
		# obj_pose.pose.orientation.w = 1.0
		# obj_pose.pose.position.x = 0
		# obj_pose.pose.position.y = 0.328		# do not modify this value
		# obj_pose.pose.position.z = 0.001/2
		# self.add_block((0.8, 1.5, 0.1), obj_pose, "table")
		# position_x.append(0)
		# position_y.append(0.328)

		entityNames= [entity.name for entity in entities]

		for filename in os.listdir(modelpath):
			objectName = filename.split('.')[0]
			filename.split('.')[0]
			idx = entityNames.index(objectName)
			associatedObject = entities[idx]

			positions, _ = self.transformToRobot([associatedObject.pose.position.x,associatedObject.pose.position.y-0.06,0])

			# print('positions')
			# print(positions)
			obj_pose.pose.position.x = positions[0]
			obj_pose.pose.position.y = positions[1]
			obj_pose.pose.position.z = 0
			
			print("spawning", objectName)
			print(os.path.join(modelpath,filename))
			# if objectName == "Spoon" or objectName == "Milk Jug" or objectName == "Spoon Holder":
			self.scene_interface.add_mesh(filename.split('.')[0], obj_pose, os.path.join(modelpath,filename), size=(0.001, 0.001, 0.001))
			# else:
				# self.scene_interface.add_mesh(filename.split('.')[0], obj_pose, os.path.join(modelpath,filename))

			position_x.append(associatedObject.pose.position.x)
			position_y.append(associatedObject.pose.position.y)
			objectNames.append(filename.split('.')[0])

			a,b,c,d = 0, 0, 0, 0


		return position_x, position_y, objectNames

	def transformToRobot(self, trans):
			
		desiredPose = np.identity(4)
		desiredPose[0:3,3] = trans

		# get offset rotation
		r = R.from_rotvec([0, 0, -(np.pi)/4])
		offsetRotation = r.as_dcm()
		offsetMatrix = np.identity(4)
		offsetMatrix[0:3,0:3] = offsetRotation

		# new position
		adjustedPose = np.matmul(offsetMatrix, desiredPose)

		# new orientation
		r = R.from_dcm(adjustedPose[0:3,0:3])
		adjustedOrientation = r.as_quat()
			
		return adjustedPose[0:3,3], adjustedOrientation