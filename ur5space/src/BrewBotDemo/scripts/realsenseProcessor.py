 #!/usr/bin/env python
import rospy
from rospy.topics import Publisher
from sensor_msgs.msg import Image, PointCloud2, CameraInfo
from cv_bridge import CvBridge
import cv2
import numpy as np
import sensor_msgs.point_cloud2 as pc2
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
import math
from scipy.interpolate import splprep, splev
from scipy.spatial.transform import Rotation as R
import image_geometry as ig
from geometry_msgs.msg import Pose, PoseArray
from gazebo_msgs.msg import ModelStates
# import statistics as st


class tempObject:


    def __init__(self):
        self.xVals =[]
        self.yVals =[]
        self.zVals =[]

# this class processes the realsense rgbd image to detect the coloured landmarks
class RealsenseProcessor:

    # constructor for RealsenseImageProcessor class
    def __init__(self): 

        # initialise CvBridge object
        self.bridge = CvBridge()

        # initialise dictionary for hsv colour ranges
        self.colours = {
                        'Coffee Tin':       ([100, 118, 0],     [107, 255, 179]),        # CoffeeTin HSV range  (blue)  
                        # 'Milo Tin':         ([72, 85, 40],      [98, 255, 124]),         # MiloTin HSV range    (green)
                        'Milo Tin':         ([60, 85, 20],      [98, 255, 124]), 
                        'Mug':              ([0, 47, 62],       [8, 193, 177]),         # Mug HSV range         (pink)
                        'Spoon':            ([7, 201, 84],      [15, 255, 255]),        # Spoon HSV range       (orange)
                        'Milk Jug':          ([18, 106, 101],    [23, 255, 255]),        # MilkJug HSV range    (yellow)
                        'Sugar Shaker':      ([136, 16, 90],     [148, 78, 251])         # SugarShaker HSV range    (magenta)
                    }
        
        # initialise member variables
        self.imSeq = None
        self.left = 0
        self.right = 0
        self.colouredObjectsFound = {}

        self.hsv = None 
        self.rgbImg = None

        # set camera parameters
        self.cam_model = ig.PinholeCameraModel()
        self.camTransform =  np.array(  [[0.00216627,  -0.711455,   0.702728,  -0.041812],
                                        [-0.00424784, -0.70273,   -0.711444,   0.0182172],
                                        [0.999989,    -0.0014439, -0.00454445, 0.0860901],
                                        [0,            0,          0,                  1]])

        self.camTransform2 = np.identity(4)

        # camera intrinsics
        self.K = np.array([[616.5429077148438, 0.0, 320.0], 
                            [0.0, 616.5429077148438, 240.0], 
                            [0.0, 0.0, 1.0]])



        self.imgSub = rospy.Subscriber('/camera/color/image_raw', Image, self.imCallback)
        self.depthSub = rospy.Subscriber('/camera/aligned_depth_to_color/image_raw', Image, self.depthCallback)
        self.cameraInfoSub = rospy.Subscriber('/camera/aligned_depth_to_color/camera_info', CameraInfo, self.caminfoCallback)
        self.objStatePub = rospy.Publisher('/objectStates', ModelStates, queue_size=1)

        self.tempObjects = {}

        self.msgCount = 0


        # member variable to keep track of incoming RGB frame
        self.currentRGB = []

        # member variable to keep track of incoming depth frame
        self.currentDepth = []

        # member variable to make a clean copy of the RGB frame
        self.clean = []

        # member variable for HSV of RGB frame
        self.hsv =[]

        # self.detObjPub = rospy.Publisher('/model_states', model_states, )

    def caminfoCallback(self, camera_info):
        self.cam_model.fromCameraInfo(camera_info)
        self.cameraInfoSub.unregister()

    #  callback for image frame
    def imCallback(self, frame):

        # get incoming RGB frame
        self.currentRGB = self.bridge.imgmsg_to_cv2(frame, desired_encoding="bgr8")

        # make a clean copy of incomign RGB frame to draw on
        self.clean = self.currentRGB.copy()

        # DEBUG: imshow of original RGB frame
        cv2.imshow('Input RGB', self.currentRGB)

        # hwc = self.currentRGB.shape
        # self.currentRGB = self.currentRGB[0:hwc[0], int(0.28*hwc[1]):hwc[1]]
        
        # convert cvImg to hsv
        self.hsv = cv2.cvtColor(self.currentRGB, cv2.COLOR_BGR2HSV)

    def depthCallback(self, msg):

        # only do function if an image has been received
        if self.hsv is None:
            return

        # detect colour from hsv
        self.currentDepth = self.bridge.imgmsg_to_cv2(msg, desired_encoding='16UC1')
        # hwc = self.currentDepth.shape
        # depthImg = self.currentDepth

        self.detectColour()
        self.processDepth()
        self.msgCount+=1

        if self.msgCount == 100: 
            print(self.msgCount)
            self.checkSamples()
            self.msgCount = 0

      # detects which colour cylinders are present in the image
    def detectColour(self):

        # initialise output dictionary
        outputDict = {}

        # cv2.imshow("hsv", hsv)
        closings = []

        # look through colours dictionary
        for objectName, (lower, upper) in self.colours.items():

            # evan you can comment this
            lower = np.array(lower, dtype = "uint8")
            upper = np.array(upper, dtype = "uint8")
            mask = np.array(cv2.inRange(self.hsv, lower, upper))
            kernel2 = np.ones((3,3),np.uint8)
            opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel2, iterations=2)
            kernel1 = np.ones((5,5),np.uint8)
            closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel1, iterations=5)
            
            _, cnts, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
            
            # if at least one contour is detected
            if len(cnts) > 0:
   
                # convert cnts list to np array
                cnts = np.array(cnts)

                # take first contour
                c = cnts[0]

                # get leftmost and rightmost column index of contour in image
                left = tuple(c[c[:, :, 0].argmin()][0])[0]
                right = tuple(c[c[:, :, 0].argmax()][0])[0]

                top = tuple(c[c[:, :, 1].argmin()][0])[1]
                bot = tuple(c[c[:, :, 1].argmax()][0])[1]

                cX = left + int((right - left)/2)
                cY = top + int((bot - top)/2)

                # print([closing.shape, cX, cY])
                closing = np.array(closing)

                depth = self.currentDepth[cY,cX]

                # if a cylinder has been detected
                if closing.any() and depth < 1700:

                    # draw contours on image and label with colour detected
                    cv2.drawContours(self.clean, cnts[0], -1, (255,154,167), 3)
                    textmid = cv2.getTextSize(objectName, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 2)
                    xOffset = int(textmid[0][0]/2)
                    cv2.putText(self.clean,objectName ,((cX-xOffset),cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (215,234,181), 2, cv2.LINE_AA)
                    self.clean = cv2.circle(self.clean, (cX,cY), radius=0, color=(0, 0, 255), thickness=5)

                    # put left col, right col and middle row indices in output dictionary
                    outputDict[objectName] = (left, right, cX, cY)

                    # print(objectName, cX, cY)

            # cv2.imshow('Conts',cvImg)
            closing = np.array(closing)
        
            # append current mask to list of masks
            closings.append(closing)

        # display image with contours
        # cv2.imshow('Conts',cvImg)

        # bitwise and all closings
        overallClosing = closings[0]
        for closing in closings:
            overallClosing = np.bitwise_or(overallClosing, closing)

        self.clean = cv2.circle(self.clean, (320,240), radius=0, color=(0, 255, 0), thickness=5)
        cv2.imshow('Masks', overallClosing)
        cv2.imshow('clean', self.clean)
        
        cv2.waitKey(1)

        # set coloursFound member variable to output dictionary
        self.colouredObjectsFound = outputDict

    def processDepth(self):
        if len(self.colouredObjectsFound.keys()) >= 1:

            for objectName, (left, right, cX, cY) in self.colouredObjectsFound.items():
                
                # get label, and left and right edge indices
                # currentKey = key
                objectLR = (left,right)
                
                # if there is no overlap on either edge, or no overlap on left edge
                if not self.edgeOverlap(objectName, objectLR): 

                    depth = self.currentDepth[cY,cX]*0.001
                
                    coords = self.convertDepth2(cX, cY, depth)
                    x, y, z = coords
                    # coords[0] += 0.3
                    absX, absY, absZ = self.absolutePosition(coords, objectName)
                    # print(absX, absY, absZ)
                    # print(self.tempObjects.keys())
                    if objectName not in self.tempObjects.keys():
                        # obj = tempObject()
                        self.tempObjects[objectName] = tempObject()
                        # print("1")\


                    self.tempObjects[objectName].xVals.append(absX)
                    self.tempObjects[objectName].yVals.append(absY)
                    self.tempObjects[objectName].zVals.append(absZ)
                    self.tempObjects[objectName].numSamples = len(self.tempObjects[objectName].xVals)


                # if there is overlap on left, not on right
                else:
                    # print('edges overlap')
                    absX, absY, absZ = 0,0,0
                    continue

            # print("")

        # reset coloursFound dictionary
        self.colouredObjectsFound = {}

    def checkSamples(self):

        # for objectName, objectData in self.tempObjects.items():
        
        modelStates = ModelStates()
        # poseMsg = Pose()

        # print(self.tempObjects.items())
        for objectName, objectData in self.tempObjects.items():

            x = np.median(objectData.xVals)
            y = np.median(objectData.yVals)
            z = np.median(objectData.zVals)

            print(objectName , x, y, z)
            poseMsg = Pose()
            poseMsg.position.x = x
            poseMsg.position.y = y
            poseMsg.position.z = z

            modelStates.name.append(objectName)
            modelStates.pose.append(poseMsg)

        self.objStatePub.publish(modelStates)
        self.tempObjects = {}

    def convertDepth2(self, cX, cY, depth):

        # evan put stuff here
        ray = self.cam_model.projectPixelTo3dRay((cX, cY))
        
        # If the depth is not a number exit
        if math.isnan(depth):
            # print "Incomplete information on depth at this point"
            return
            
        # Otherwise mulitply the depth by the unit vector of the ray
        else:
            # print "depth", depth
            cam_coor = [depth*ray[0],depth*ray[1],depth*ray[2]]
            #print "camera frame coordinate:", cam_coor  
            return cam_coor


    def edgeOverlap(self, currentkey, currentLandmarkLR):

        # initialise variables
        edgeOverlap = False

        # check if there is overlap with the edge of the frame
        if (0 >= currentLandmarkLR[0] -10) or (640 <= currentLandmarkLR[1] +10):
            edgeOverlap = True

        # return result
        return edgeOverlap
    
    def absolutePosition(self, coords, key):
        yOffset = -0.03
        xOffset = 0.03

        x, y, z = coords
        absX = -y + xOffset
        absY = 0.68 - x + yOffset
        absZ = 1.15 - z
        # print('absolute position of', key)
        
        # print(round(absX,1), round(absY,1), round(absZ,1))
        # return absX, absY, absZ
        return round(absX,1), round(absY,1), round(absZ,1)

if __name__ == '__main__':

    # initialise node
    print('RealsenseImageProcessor Initialised')
    rospy.init_node('realsense_image_processor')
    # run node
    try:
        realsense = RealsenseProcessor()
        rospy.spin()
    except rospy.ROSInterruptException: pass
