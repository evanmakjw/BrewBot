#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PACKAGEPATH=`dirname $SCRIPTPATH`
WORKSPACE_SRC="$( cd -- "$(dirname "$PACKAGEPATH")" >/dev/null 2>&1 ; pwd -P )"
BREWBOT_SCRIPTS=$WORKSPACE_SRC"/BrewBotDemo/scripts"
# echo $BREKKIEDRINKS
# echo $WORKSPACE_SRC

printf '\n\e[1;36m%-6s\e[m\n' "Operating Modes:"
echo '---------------------'
printf '1 - Simulation\n'
printf '2 - Demonstration\n'
echo '---------------------'
printf '\n\e[1;36m%-6s\e[m' "Choose operation mode: "
read -n1 OP_MODE

VAR1="1"
VAR2="2"

if [ "$OP_MODE" = "$VAR1" ]
then
    printf '\n\e[1;34m%-6s\e[m' "sim ur5 Bringup"
    gnome-terminal --tab -t "sim ur5 Bringup" -- bash -ic "roslaunch BrewBotDemo BrewBotSim.launch; exec bash" 
    sleep 10

    printf '\n\e[1;34m%-6s\e[m' "Initialising moveit Planner (3/8)"   
    gnome-terminal --tab -t "moveit Planner" -- bash -ic "roslaunch ur5_moveit_config ur5_moveit_planning_execution.launch sim:=true; exec bash" 
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising Rviz (4/8)"
    gnome-terminal --tab -t "Rviz" -- bash -ic "roslaunch ur5_moveit_config moveit_rviz.launch config:=true; exec bash" 
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising realsense D435 (5/8)"
    gnome-terminal --tab -t "realsense D435" -- bash -ic "roslaunch realsense2_camera rs_d435_camera_with_model.launch; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising Ordering Interface (6/8)"
    gnome-terminal --tab -t "OrderingInterface" -- bash -ic "roslaunch rosbridge_server rosbridge_websocket.launch; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising realsenseDataProcessor (7/8)"
    gnome-terminal --tab -t "realsenseDataProcessor" -- bash -ic "cd ${BREWBOT_SCRIPTS} && python realsenseProcessor.py; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising BreakfastDrinks:D (8/8)"
    gnome-terminal --title="BreakfastDrinks:D" -- bash -ic "cd ${BREWBOT_SCRIPTS} && python breakfastDrinks.py; exec bash"
    sleep 5

    printf '\n\e[1;35m%-6s\e[m\n' "Launching Completed!"

elif [ "$OP_MODE" == "$VAR2" ]
then
    printf '\n\e[1;36m%-6s\e[m' "Robot ip: "
    read ROBOT_IP_ADDRESS

    printf '\n\e[1;34m%-6s\e[m' "Initialising gazebo (1/9)"
    gnome-terminal --tab -t "gazebo" -- bash -ic "roslaunch BrewBotDemo BrewBotDemo.launch; exec bash" 
    sleep 10

    ROBOT_CONFIG_FILE="${WORKSPACE_SRC}/my_configuration_robot.yaml"
    printf '\n\e[1;34m%-6s\e[m' "Creating ${ROBOT_CONFIG_FILE} (2/9)"
    gnome-terminal --tab -t "Creating yaml" -- bash -ic "roslaunch ur_calibration calibration_correction.launch robot_ip:=${ROBOT_IP_ADDRESS} target_filename:=${ROBOT_CONFIG_FILE}; exec bash" 
    sleep 10 

    printf '\n\e[1;34m%-6s\e[m' "Initialising ur5 BringUp (3/9)"
    gnome-terminal --tab -t "ur5 BringUp" -- bash -ic "roslaunch ur_robot_driver ur5_bringup.launch robot_ip:=${ROBOT_IP_ADDRESS} kinematics_config:=${ROBOT_CONFIG_FILE} limited:=true; exec bash" 
    sleep 5

    # need to wait until robot bringup is done to start robot from tablet
    printf '\n\e[1;36m%-6s\e[m' "Press enter after starting robot from tablet: "
    read temp

    printf '\e[1;34m%-6s\e[m' "Initialising moveit Planner (4/9)"
    gnome-terminal --tab -t "moveit Planner" -- bash -ic "roslaunch ur5_moveit_config ur5_moveit_planning_execution.launch sim:=false; exec bash" 
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising Rviz (5/9)"
    gnome-terminal --tab -t "Rviz" -- bash -ic "roslaunch ur5_moveit_config moveit_rviz.launch config:=true; exec bash" 
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising realsense D435 (6/9)"
    gnome-terminal --tab -t "realsense D435" -- bash -ic "roslaunch realsense2_camera rs_d435_camera_with_model.launch; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising Ordering Interface (7/9)"
    gnome-terminal --title="OrderingInterface" -- bash -ic "roslaunch rosbridge_server rosbridge_websocket.launch; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising realsenseDataProcessor (8/9)"
    gnome-terminal --tab -t "realsenseDataProcessor" -- bash -ic "cd ${BREWBOT_SCRIPTS} && python realsenseProcessor.py; exec bash"
    sleep 5

    printf '\n\e[1;34m%-6s\e[m' "Initialising BreakfastDrinks:D (9/9)"
    gnome-terminal --title="BreakfastDrinks:D" -- bash -ic "cd ${BREWBOT_SCRIPTS} && python breakfastDrinks.py; exec bash"
    sleep 5



    printf '\n\e[1;35m%-6s\e[m\n' "Launching Completed!"
fi


