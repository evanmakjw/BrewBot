# BrewBotCode

Code for BrewBot Major Project can be found in the **ur5space/src/BrewBotDemo/src** directory

To run the project:
- `cd ur5space/src/BrewBotDemo/launch`
- `chmod +x BrewBotDemo.sh`
- `./BrewBotDemo`
